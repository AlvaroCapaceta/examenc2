const cambio = {
  1: 19.85,
  2: 1,
  3: 1.35,
  4: 0.99,
};

const monedas = {
  1: "Peso Mexicano",
  2: "Dolar Estadounidense",
  3: "Dolar Canadiense",
  4: "Euro",
};

var acumulado = 0;

const change = () => {
  limpiarCampos();
  let opt = document.getElementById("cbxDestino");
  let moneda = document.getElementById("cbxOrigen").value;

  switch (moneda) {
    case "1":
      opt.options[0] = new Option("Dolar Estadounidense", 2);
      opt.options[1] = new Option("Dolar Canadiense", 3);
      opt.options[2] = new Option("Euro", 4);
      break;
    case "2":
      opt.options[0] = new Option("Peso Mexicano", 1);
      opt.options[1] = new Option("Dolar Canadiense", 3);
      opt.options[2] = new Option("Euro", 4);
      break;
    case "3":
      opt.options[0] = new Option("Peso Mexicano", 1);
      opt.options[1] = new Option("Dolar Estadounidense", 2);
      opt.options[2] = new Option("Euro", 4);
      break;
    case "4":
      opt.options[0] = new Option("Peso Mexicano", 1);
      opt.options[1] = new Option("Dolar Estadounidense", 2);
      opt.options[2] = new Option("Dolar Canadiense", 3);
      break;
  }
};

const calcular = () => {
  let cantidad = document.getElementById("txtCantidad").value;
  let monedaOrigen = document.getElementById("cbxOrigen").value;
  let monedaDestino = document.getElementById("cbxDestino").value;
  let subtotal = 0;
  let dolares = 0;

  if (cantidad <= 0) {
    alert("Ingrese una cantidad correcta");
    return;
  }

  cantidad = parseFloat(cantidad);
  dolares = cantidad / cambio[monedaOrigen];
  subtotal = dolares * cambio[monedaDestino];
  document.getElementById("txtSubtotal").value = subtotal.toFixed(4);

  let comision = cantidad * 0.03;
  let totalPagar = cantidad * 1.03;
  document.getElementById("txtComision").value = comision.toFixed(4);
  document.getElementById("txtTotal").value = totalPagar.toFixed(4);
};

const limpiarCampos = () => {
  document.getElementById("txtComision").value = "";
  document.getElementById("txtSubtotal").value = "";
  document.getElementById("txtTotal").value = "";
};

const registrar = () => {
  let monto = document.getElementById("txtTotal").value;

  if (monto <= 0) {
    alert("Primero calcule el total a pagar");
    return;
  }

  monto = parseFloat(monto);
  acumulado += monto;

  document.getElementById("lblTotalGeneral").innerHTML =
    "Total General: " + acumulado;
  let cantidad = document.getElementById("txtCantidad").value;
  let mOrigen = document.getElementById("cbxOrigen").value;
  let mDestino = document.getElementById("cbxDestino").value;
  let parrafo = document.getElementById("parrafoRegistros");
  let comision = document.getElementById("txtComision").value;
  let subtotal = document.getElementById("txtSubtotal").value;
  let total = document.getElementById("txtTotal").value;
  parrafo.innerHTML +=
    "<br/>" +
    cantidad +
    " " +
    monedas[mOrigen] +
    " a " +
    monedas[mDestino] +
    " " +
    subtotal +
    " " +
    comision +
    " " +
    total;
};

const borrar = () => {
  document.getElementById("parrafoRegistros").innerHTML =
    "--------------------";
  document.getElementById("lblTotalGeneral").innerHTML = "Total General: ";
  acumulado = 0;
};
